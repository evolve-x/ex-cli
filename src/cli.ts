#!/usr/bin/env node

import program from 'commander';
import Hello from './Commands/Hello';
import Configure from './Commands/Configure';
import {  ConfiguratiunFile } from './Utils/Utils';
import fs from 'fs-extra';
import superagent from 'superagent';
import os from 'os';
import Signup from './Commands/Signup';
import GenEVConfig from './Commands/GenEVConfig';
import Account from './Commands/Account';
import Info from './Commands/Info';

let config: ConfiguratiunFile | null;
let userLevel = 0;

const version = 'BETA 3';

program.version(version);

let configure;

function sendInfo(user: any) {
    console.log(`Evolve-X CLI.\nVersion ${version}`);
    if (user) {
        console.log(`\nHello ${user.username[0].toUpperCase() + user.username.slice(1)}!\nSecurity level: ${userLevel}`);
    }
    program.outputHelp();
    console.log('\nhttps://gitlab.com/evolve-x/ex-cli');
    console.log('Copyright (c) VoidNulll 2020');
    process.exit(0);
}

async function loadUser() {
    let user: any;
    if (os.platform() === 'linux' && fs.existsSync(`${os.userInfo().homedir}/.evolve-x.json`) ) {
        config = require(`${os.userInfo().homedir}/.evolve-x.json`) as ConfiguratiunFile;
        if (config.password && config.username && config.url) {
            try {
                user = await superagent.get(`${config.url}/api/account`).set('username', config.username).set('password', config.password);
                user = user.body;
                if (user.owner) {
                    userLevel = 3;
                } else if (user.admin) {
                    userLevel = 2;
                } else {
                    userLevel = 1;
                }
                return user;
            } catch (e) {
                if (e.status === 401) {
                    console.log(`User authorization failed with status code 401`);
                }
            }
        }
    }
}

(async function initCmds() {
    const user = await loadUser();

    const HelloCmd = new Hello(program);
    await HelloCmd.init();

    configure = new Configure(userLevel, program);
    const hook = configure.preHook();
    if (hook) {
        await configure.init();
    }
    const signup = new Signup(userLevel, program);
    const shook = signup.preHook();
    if (shook) {
        await signup.init();
    }

    const genconfig = new GenEVConfig(program);
    await genconfig.init();

    const accountCMD = new Account(userLevel, program);
    const acchook = accountCMD.preHook();
    if (acchook) {
        await accountCMD.init();
    }

    const info = new Info(userLevel, program);
    const infohook = info.preHook();
    if (infohook) {
        await info.init();
    }

    if (!process.argv.slice(2).length) {
        sendInfo(user);
    }

    program.on('command:*', () => {
        console.error('Invalid command: %s\nSee --help for a list of available commands.', program.args.join(' '));
        process.exit(1);
    });
    program.parse(process.argv);
}() );

process.on('SIGTSTP', () => {
    console.log('[FATAL] Do not kill the CLI!');
    process.exit(1);
} );

process.on('SIGINT', () => {
    console.log('[FATAL] Do not kill the CLI!');
    process.exit(1);
} );