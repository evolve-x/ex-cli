import Command from '../Structures/Command';
import superagent from 'superagent';
import fs from "fs-extra";
import os from "os";
import Utils, { ConfiguratiunFile } from '../Utils/Utils';

export default class Account extends Command {
    private config?: ConfiguratiunFile;

    constructor(userLevel: number, commander: any) {
        super(userLevel, commander);
        this.label = 'account';
        this.description = 'Show your account info.';
        this.levelNeeded = 1;
    }

    preHook() {
        const config = Utils.getConfig();
        if (config) {
            this.config = config;
            return true;
        }
        return false;
    }

    async execute() {
        if (!this.config) {
            console.log('[FATAL] No config present!');
            process.exit();
        }
        try {
            let user: any = await superagent.get(`${this.config.url}/api/account`).set('username', this.config.username).set('password', this.config.password);
            user = user.body;
            let userLevel = 1;
            if (user.owner) {
                userLevel = 3;
            } else if (user.admin) {
                userLevel = 2;
            } else {
                userLevel = 1;
            }
            const ulvlObj = { 1: 'User', 2: 'Service Administrator', 3: 'Service Owner' };
            // @ts-ignore
            console.log(`\n[ACCOUNT}\n\n    Username: ${user.username}\n    User ID: ${user.uID}\n    Security Level: ${userLevel}\n    Title: ${ulvlObj[userLevel]}\n    Images Uploaded: ${user.images}\n    Links Shortened: ${user.shorts}\n`);
            process.exit();
        } catch (e) {
            if (e.status === 401) {
                console.log(`User authorization failed with status code 401`);
            }
        }
    }
}