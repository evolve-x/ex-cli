import Command from '../Structures/Command';
import RL from '../Utils/Readline';
import superagent from 'superagent';
import readline from 'readline-sync';
const rl = new RL();
import os from 'os';
import fs from 'fs-extra';
import Utils from '../Utils/Utils';

export default class Conmfigure extends Command {
    constructor(level: number, commander: any) {
        super(level, commander);
        this.label = 'config <url>';
        this.description = 'Configure your Evolve-X instance file';
        this.levelNeeded = 0;
    }

    preHook() {
        return !Utils.getConfig();
    }

    async execute(serial: any): Promise<void | any> {
        if (!serial || serial.match(/https:\/\/|http:\/\//) ) {
            console.log('Error, provide a URL ("https://" is automatically added)!');
            process.exit(1);
        }
        try {
            await superagent.get(`https://${serial}/api`);
        } catch (e) {
            if (e.errno && (e.errno === 'ENOTFOUND' || e.errno === 'ECONNREFUSED')) {
                console.log(`Error, URL "${serial}" not found or invalid!`);
            } else if (e.status && e.status === 404) {
                console.log(`Error, URL "${serial}" is not a valid Evolve-X URL`);
            } else {
                console.log(`Unexpected Error: ${e}`);
            }
            process.exit(1);
        }
        const out = await rl.question(`Is https://${serial} the URL you want?`, ['yes', 'no']);
        if (out === 'no') {
            console.log('Ok, try again. Goodbye!');
            process.exit(0);
        }
        const username = await rl.question('What is your username?');
        const uConfirm = await rl.question(`Is ${username} the username you want this to be listed under?`, ['yes', 'no']);
        if (uConfirm === 'no') {
            console.log('You will have to retry then, goodbye.');
            process.exit(0);
        }
        const password = readline.questionNewPassword('What is your password? ', {confirmMessage: "Confirm password: ", min: 8, max: 32});
        await fs.writeFile(`${os.userInfo().homedir}/.evolve-x.json`, JSON.stringify({ url: `https://${serial}`, password, username }) );
        console.log('Configuration complete!');
    }
}