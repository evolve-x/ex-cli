import Command from '../Structures/Command';
import RL from '../Utils/Readline';
import superagent from 'superagent';
import readline from 'readline-sync';
const rl = new RL();
import os from 'os';
import fs from 'fs-extra';

export default class Signup extends Command {
    constructor(level: number, commander: any) {
        super(level, commander);
        this.label = 'signup <url>';
        this.description = 'Sign up for an Evolve-X instance';
        this.levelNeeded = 0;
    }

    preHook() {
        if (fs.existsSync(`${os.userInfo().homedir}/.evolve-x.json`)) {
            return false;
        }
        return os.platform() === 'linux';
    }

    async execute(url: string) {
        if (!url || url.match(/https:\/\/|http:\/\//) ) {
            console.log('Error, provide a URL ("https://" is automatically added)!');
            process.exit(1);
        }
        try {
            await superagent.get(`https://${url}/api`);
        } catch (e) {
            if (e.errno && (e.errno === 'ENOTFOUND' || e.errno === 'ECONNREFUSED')) {
                console.log(`Error, URL "${url}" not found or invalid!`);
            } else if (e.status && e.status === 404) {
                console.log(`Error, URL "${url}" is not a valid Evolve-X URL`);
            } else {
                console.log(`Unexpected Error: ${e}`);
            }
            process.exit(1);
        }

        const out = await rl.question(`Is https://${url} the URL you want to signup for?`, ['yes', 'no']);
        if (out === 'no') {
            console.log('Ok, try again. Goodbye!');
            process.exit(0);
        }
        console.log('\n- Input "force_end" as username to end the process -\n');
        const username = await this.getUsername();
        console.log('\n- Input "force_end" as the password to end the process -\n');
        const password = this.getPassword();

        try {
            const success = await superagent.post(`https://${url}/api/signup`).send({ username, password });
            if (success.status === 201) {
                console.log('[SUCCESS] Signup request sent!');
                process.exit(0);
            } else if (success.status === 423) {
                console.log('[FAIL] Signups locked!');
                process.exit(0);
            } else if (success.status === 400) {
                console.log('[FAIL] Bad request.');
                process.exit(0);
            }
        } catch (e) {
            if (e.status) {
                if (e.status === 423) {
                    console.log('[FAIL] Signups locked!');
                    process.exit(0);
                } else if (e.status === 400) {
                    console.log('[FAIL] Bad request.');
                    process.exit(0);
                } else {
                    console.log('[FAIL] Request failed with unknown code');
                    process.exit(0);
                }
            } else {
                console.log(`Error: ${e}`);
                process.exit(0);
            }
        }
        console.log('CLI did something unexpected');
        process.exit(0);
    }

    async getUsername(): Promise<string> {
        const username = await rl.question('What do you want your username to be? ');

        if (username === 'force_end') {
            console.log('Force end received, exiting...');
            process.exit(0);
        }
        const uConfirm = await rl.question(`Just to confirm, you said you wanted your username to be ${username}`, ['yes', 'no']);
        if (uConfirm === 'no') {
            console.log('Try again. Input one you want.');
            return this.getUsername();
        }
        return username;
    }

    getPassword(): string {
        const password = readline.questionNewPassword('What would you like your password to be? ', { min: 8, max: 32, confirmMessage: 'Confirm password: ', charlist: '$<a-z><A-Z><0-9>._&' });
        if (password === 'force_end') {
            console.log('Force end received, exiting...');
            process.exit(0);
        }
        const match: RegExpMatchArray | null = password.match(/[A-Za-z0-9_.&]/g);
        if (!match || match.length !== password.length) {
            console.log('Password must include only alphanumeric characters. "_", ".", and "&"');
            return this.getPassword();
        }
        return password;
    }
}