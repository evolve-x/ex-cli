import Command from '../Structures/Command';

export default class Hello extends Command {
    constructor(commander: any) {
        super(0, commander);
        this.label = 'hello';
        this.description = 'Hello World! Test command';
        this.levelNeeded = 0;
        this.options = [ { label: '-d, --different', description: 'Different output' } ]
    }

    execute(serial: any): void|any {
        if (serial.different) {
            console.log('Hallo Welt!');
        } else {
            console.log('Hello World!');
        }
    }
}