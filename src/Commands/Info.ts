import Command from '../Structures/Command';
import superagent from 'superagent';
import fs from "fs-extra";
import os from "os";
import moment from 'moment';
import Utils from "../Utils/Utils";

export default class Info extends Command {
    private config?: { username: string; password: string; url: string };

    constructor(userLevel: number, commander: any) {
        super(userLevel, commander);
        this.label = 'info';
        this.description = 'Show instance info.';
        this.levelNeeded = 1;
    }

    preHook() {
        const config = Utils.getConfig();
        if (config) {
            this.config = config;
            return true;
        }
        return false;
    }

    async execute() {
        if (!this.config) {
            console.log('[FATAL] No config!');
            process.exit(0);
        }
        try {
            let i: any = await superagent.get(`${this.config.url}/api`);
            try {
                i = JSON.parse(i.text);
            } catch (e) {
                console.log('\n[INSTANCE INFO]\nVersion: Unknown');
                process.exit();
            }
            let str = `Version: ${i.version}\nNode Version: ${i.nodeVersion}\nUptime: ${i.uptime}\nOnline Since: ${moment(i.onlineSince).format('dddd, MMMM Do YYYY, h:mm:ss a')}\n`;
            if (i.shards) {
                str += `Shards Running: ${i.shards}\n`;
            }
            console.log(`\n[INSTANCE INFO FOR ${this.config.url}]\n\n${str}`);
            process.exit(0);
        } catch(e) {
            console.log('Request for info failed\nThe instance may be too out of date, not online,. or not even be an Evolve-X instance');
            process.exit(0);
        }
    }
}