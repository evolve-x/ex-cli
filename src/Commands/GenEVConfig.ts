import Command from '../Structures/Command';
import RL from '../Utils/Readline';
import superagent from 'superagent';
import readline from 'readline-sync';
const rl = new RL();
import fs from 'fs-extra';
import { join } from 'path';

interface CertOptions {
    key?: string | any;
    cert?: string | any;
}

export interface SharderOptions {
    enabled: boolean;
    maxCores: number;
    maxMemory: string;
}


interface Options {
    port?: number;
    url?: string;
    mongoUrl?: string;
    signups?: boolean;
    apiOnly?: boolean;
    trustProxies?: boolean;
    certOptions?: CertOptions;
    sharder?: SharderOptions;
}


export default class GenEVConfig extends Command {
    constructor(program: any) {
        super(0, program);
        this.label = 'genconfig';
        this.description = 'Generate an Evolve-X config';
    }

    isBooleanInput(input: any, extender?: string[]) {
        const arr = extender ? ['no', 'n', 'true', 'yes', 'false', 'y'].concat(extender) : ['no', 'n', 'true', 'yes', 'false', 'y'];
        return !(typeof input !== 'boolean' && !arr.includes(input));

    }

    isURL(input: any) {
        return !(!input.split('.') || !input.split('.')[1]);

    }

    getFileToSave(): Promise<string> {
        const dir = readline.questionPath('What directory would you like this to be in (absolute path)? ', { limit: (input) => {
                return fs.existsSync(input);
            }, limitMessage: 'Not a directory! Try again.' });
        const file = readline.question('What would you like the file to be called (".json" will be added automatically)? ', { limitMessage: 'File exists', limit: (input) => {
                return !fs.existsSync(join(dir, `${input}.json`));
            } });
        const check = !readline.question(`Is ${join(dir, `${file}.json`)} the file you wanted? `, { falseValue: ['no', 'n', 'false'], trueValue: ['yes', 'y', 'yes'], limitMessage: 'This is a true or false question. Try again. ', limit: this.isBooleanInput });
        if (!check) {
            return this.getFileToSave();
        }
        return Promise.resolve(join(dir, `${file}.json`));
    }

    configureCerts(): CertOptions {
        const key = readline.questionPath('What is the path to the PEM private key? ');
        const cert = readline.questionPath('What is the path to the certificate chain/certificate? ');
        return { key, cert };
    }

    async execute() {
        const port = readline.question('\nWhat port would you like to listen on: ', { charlist: '$<0-9>', limit: (input) => {
                return !(isNaN(Number(input)) || input.length > 5 || Number(input) === 0 || Number(input) > 65535 || Number(input) < 1);
            }, limitMessage: '\nThe port must be above 1, amd below 65535'
        });
        const aPort = Number(port);
        const signups = !!readline.question('\nAllow signups? ', { trueValue: ['yes', 'y', 'true'], falseValue: ['no', 'n', 'false'], limitMessage: '\nWould you like signups to be enabled? It is a true or false/yes or no question. ', limit: this.isBooleanInput });
        const url = readline.question('\nURL: ', { limitMessage: '\nNot a URL! Input a URL', limit: this.isURL, caseSensitive: false });
        const mongoUrl = readline.question('\nMongoDB URL ("no" = no url) ', { limitMessage: '\nEither do not have a url, or input a valid URL', falseValue: ['no'], limit: (input) => {
                return (input !== 'no' || !this.isURL(input) );
            }, caseSensitive: false });
        const apiOnly = !!readline.question('\nDisable frontend? ', { falseValue: ['no', 'n', 'false'], trueValue: ['yes', 'y', 'yes'], limitMessage: '\nThis is a true or false question. Try again. ', limit: this.isBooleanInput });
        const trustProxies = !!readline.question('\nTrust (reverse) proxies such as NGINX or Apache? ', { falseValue: ['no', 'n', 'false'], trueValue: ['yes', 'y', 'yes'], limitMessage: '\nThis is a true or false question. Try again. ', limit: this.isBooleanInput });
        const configCerts = !!readline.question('\nWould you like to configure the certificates or no? ', { falseValue: ['no', 'n', 'false' ], trueValue: ['yes', 'y', 'yes'], limitMessage: '\nThis is a true or false question. Try again. ', limit: (input) => this.isBooleanInput(input) });
        const config: Options = { trustProxies, port: aPort, url, signups, mongoUrl, apiOnly };
        if (configCerts) {
            config.certOptions = this.configureCerts();
        }
        const printToFile = !!readline.question('\nWould you like me to save this to a file? ', { falseValue: ['no', 'n', 'false'], trueValue: ['yes', 'y', 'yes'], limitMessage: '\nThis is a true or false question. Try again. ', limit: this.isBooleanInput });
        if (printToFile) {
            const file = await this.getFileToSave();
            await fs.writeFile(file, JSON.stringify(config, null, 4));
            console.log(`\n[SUCCESS] Wrote config to ${file}`);
        }
        if (!config.mongoUrl) {
            delete config.mongoUrl;
        }
        if (!config.trustProxies) {
            delete config.trustProxies;
        }
        console.log('\nConfig generation done!\n\n');
        console.log(JSON.stringify(config, null, 4));
        process.exit(0);
    }
}