import *as Commander from 'commander';

interface Option {
    label: string;
    description: string;
}

export default class Command {
    public label: string;

    public description: string;

    public options: Option[];

    public levelNeeded: number;

    public level: number;

    private commander: any;

    public reqArgs: string;

    public maxLevel?: number;

    constructor(level: number, commander: any) {

        this.label = '';
        this.description = '';
        this.options = [];

        this.levelNeeded = 0;
        this.level = level;
        this.maxLevel;

        this.commander = commander;
        this.reqArgs = '';
    }

    init() {
        let lvl;
        if (this.maxLevel && this.level > this.maxLevel) {
            lvl = true;
        }
        if (!(this.levelNeeded > this.level)) {
            lvl = true;
        }
        if (lvl) {
            let str = this.label;
            if (this.reqArgs) {
                str += this.reqArgs;
            }
            const command = this.commander.command(str).description(this.description)
            if (this.options && this.options.length > 0) {
                for (const opt of this.options) {
                    command.option(opt.label, opt.description)
                }
            }
            command.action(this.execute.bind(this));
        }
    }

    execute(serial?: any, cmd?: any) {
        throw Error('[NOT IMPLEMENTED]');
    }
}