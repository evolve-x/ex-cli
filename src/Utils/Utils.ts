import os from "os";
import fs from 'fs-extra';

export interface ConfiguratiunFile {
    username: string;
    password: string;
    url: string;
}

export default class Utils {

    static getConfig(): ConfiguratiunFile | false | undefined {
        // // CLI configuration structure is designed for linux, maybe macOS but I do not know macOS's file structure
        if (os.platform() !== 'linux') {
            return false;
        }
        // So, the configuration needs to exist to get it.
        if (!fs.existsSync(`${os.userInfo().homedir}/.evolve-x.json`) ) {
            return undefined;
        }
        // Return the configuration
        return require(`${os.userInfo().homedir}/.evolve-x.json`);
    }

    static writeConfig(newConfig: ConfiguratiunFile, flags?: string[]): ConfiguratiunFile | undefined | boolean {
        // CLI configuration structure is designed for linux, maybe macOS but I do not know macOS's file structure
        if (os.platform() !== 'linux') {
            return false;
        }
        // Check if the update flag has been passed and if the file exists
        if (fs.existsSync(`${os.userInfo().homedir}/.evolve-x.json`) && flags && flags.length > 0 && !flags.find(flag => flag === '--update' || flag === '-u') ) { // If file exists and its not updating, return undefined.
            return undefined;
        }
        // Write the new configuration to the users config file
        fs.writeJsonSync(`${os.userInfo().homedir}/.evolve-x.json`, newConfig, {spaces: 4});
        return fs.readJsonSync(`${os.userInfo().homedir}/.evolve-x.json`);
    }
}